# README #

Making a docker image with which we could start checking and executing the [jsnark](https://github.com/akosba/jsnark)'s codes.

### Updated ###

* Ubuntu 16.10
* OpenJava

### Note ###

Couldnt use the jsnark original src because the submoduled libsnark had some errors("std::iota couldnt be found"), so I forked libsnark individually and cherry-picked some needed commits from akosba's [libsnark repo](https://github.com/akosba/libsnark) because "run_libsnark" was added by him.