FROM ubuntu:16.10
ARG DEBIAN_FRONTEND=noninteractive
MAINTAINER Takeshi Nakano <tf0054@gmail.com>

ENV HOME /root

# add Mirror site nearby berlin
RUN sed -i~ -e 's/archive.ubuntu.com/ubuntu.mirror.lrz.de/' /etc/apt/sources.list && \
    apt-get update

RUN apt-get install -y software-properties-common apt-utils

RUN apt-get update && \
    apt-get install -y build-essential \
                       # zlib1g-dev libssl-dev libreadline-dev libyaml-dev \
                       # libxml2-dev libxslt-dev sqlite3 libsqlite3-dev \
                        libgmp3-dev libprocps-dev libgtest-dev \
                        python-markdown libboost-all-dev libssl-dev junit4 \
                       vim git sudo zip unzip tree python \
                       net-tools iputils-ping dnsutils telnet wget curl \
                       # emacs25 silversearcher-ag sshfs \
                       openjdk-8-jdk openssh-server

WORKDIR /root

# jsnark
RUN git clone --recursive https://github.com/tf0054/jsnark.git
RUN cd jsnark/libsnark && ./prepare-depends.sh && make NO_SUPERCOP=1 NO_DOC=1

# ssh
RUN sed -ri 's/UsePAM yes/UsePAM no/g' /etc/ssh/sshd_config
RUN mkdir -p /var/run/sshd && chmod 755 /var/run/sshd
ADD id_rsa.pub /root/.ssh/authorized_keys
RUN chmod -R g-rwx,o-rwx /root/.ssh

# Clojure
RUN curl -s https://raw.githubusercontent.com/technomancy/leiningen/2.7.1/bin/lein > \
    /usr/local/bin/lein && \
    chmod 0755 /usr/local/bin/lein

# Clean
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Add a normal user
RUN useradd -p tcuser -G sudo -s /bin/bash -d /work/tf0054 tf0054
RUN ln -s /work/tf0054 /home/tf0054

# modify sudoers
# Enable passwordless sudo for users under the "sudo" group 
RUN sed -i.bkp -e \
    's/%sudo\s\+ALL=(ALL\(:ALL\)\?)\s\+ALL/%sudo ALL=NOPASSWD:ALL/g' \
    /etc/sudoers

# Exec
EXPOSE 22
ENTRYPOINT [ "/usr/sbin/sshd", "-D" ]
